void main(){
  //Initialise the variables
  String myName ="Lerato";
  String favouriteApplication = "Netflix";
  String favouriteCity = "Johannesburg";

  //Print all the variables to the console
  print("My name is $myName");
  print("My favourite application is $favouriteApplication");
  print("I like $favouriteCity with all my heart");
}